var resolution = {
    width: 1024,
    height: 768
};

var serverURL = casper.cli.get('serverURL');
var timeout = 5000;
var img_count = 0;
var imageType = '.png';
var screenShotPath = 'screenshots_' + resolution.width + "x" + resolution.height + '/';


var getImageName = function(name) {
    var fileName = screenShotPath;
    fileName += (img_count < 10) ? '0' + img_count : img_count;
    img_count++;
    fileName += '_' + name + imageType;
    return fileName;
};

var captureImage = function(name) {
    var fileName = getImageName(name);
    casper.viewport(resolution.width, resolution.height);
    casper.capture(fileName);
};

casper.options.viewportSize = {width: resolution.width, height: resolution.height};
casper.options.logLevel = "debug";
casper.options.verbose = false;

casper.on('page.error', function (msg, trace) {
    this.echo('Error: ' + msg, 'ERROR');
    for (var i = 0; i < trace.length; i++) {
        var step = trace[i];
        this.echo('   ' + step.file + ' (line ' + step.line + ')', 'ERROR');
    }
});

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g, '');
};

String.prototype.ltrim = function() {
	return this.replace(/^\s+/, '');
};

String.prototype.rtrim = function() {
	return this.replace(/\s+$/, '');
};

String.prototype.fulltrim = function() {
	return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' ');
};