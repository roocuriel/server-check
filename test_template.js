/*
*
* CasperJS empty skeleton, use folder format XX_feature name, ie: 01_dashboard, this will ensure all tests run in order
* ( because we need to login before testing anything else)
*
* run casper test in command line:
* casperjs test --web-security=no --ignore-ssl-errors=yes --includes=common/common_functions.js tests --serverURL=<url for server>
*
* example:
* casperjs test --web-security=no --ignore-ssl-errors=yes --includes=common/common_functions.js tests --serverURL=https://54.87.153.42
*
* documentation can be found here:
* http://casperjs.readthedocs.org/en/latest/modules/index.html
*
*/

casper.test.begin('<Enter your test description here>', function (test) {

    var dashboardURL = serverURL + '/#/'; //this is the dashboard path, if you need something else, change here
    casper.start(dashboardURL, function () {
        console.log('# Starting tests on ' + this.getCurrentUrl());
    });

    casper.then(function () {
        this.waitForSelector('#dashboard-container', function () {
            // what do we want to test ?
            // use a significant name, remember we are running test on command line
            test.assertExists('#dashboard-container', 'Dashboard loaded');

            // capture image on success
            captureImage('dashboard_loaded');
        }, function () {
            // capture image on failure
            captureImage('dashboard_failed');
            // why did the test failed?
            // use a significant name, remember we are running test on command line
            test.assertExists('#dashboard-container', 'Dashboard failed');
        }, timeout);
    });

    casper.run(function () {
        test.done();
    });
});