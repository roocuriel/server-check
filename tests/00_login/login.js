// DEV https://54.226.52.209/#/login
// QA https://54.87.153.42/#/login

casper.test.begin('TS5 Portal Login Test', function (test) {

    var loginURL = serverURL + '/#/login';

    casper.start(loginURL, function () {
        console.log('# Starting tests on ' + this.getCurrentUrl());
    });

    casper.then(function () {
        this.waitForSelector('.login-username', function () {
            test.assertUrlMatch(/login/, 'Login screen loaded');
            test.assertExists('.login-username', 'username field found');
            test.assertExists('.login-password', 'password field found');
            captureImage('login_screen_loaded');
        }, function () {
            captureImage('login_screen_failed');
            test.assertUrlMatch(/login/, 'Login screen loaded');
        }, timeout);
    });

    casper.then(function () {
        this.sendKeys('.login-username', 'admin');
        this.sendKeys('.login-password', 'eGate@TS5');
        test.assertExists('.login-submit', 'submit button found');
        captureImage('login_screen_before_submit');
        this.click('.login-submit');
    });

    casper.then(function () {
        this.waitForSelector('#dashboard-container', function () {
            test.assertExists('#dashboard-container', 'Dashboard loaded');
            captureImage('dashboard_loaded');
        }, function () {
            captureImage('dashboard_failed');
            test.assertExists('#dashboard-container', 'Dashboard failed');
        }, timeout);
    });

    casper.run(function () {
        test.done();
    });
});